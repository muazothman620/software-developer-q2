<?php

namespace App\Http\Controllers;

use App\Models\company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'name' => 'required|min:5',
            'email' => 'required|email',
            'logo' => 'required|mimes:png',
            'link' => 'required|url'

        ]);

        if($request->hasFile('logo')){
            
            $filename = $request->name.'.'.$request->file('logo')->getClientOriginalExtension();
            $request->logo =  $filename;
            
            $file = $request->file('logo');
            $file->move('uploads/', $filename);
            
        }else{
            
            $request->logo = '';
        }

        company::create([
            'name' => $request->name,
            'email' => $request->email,
            'link' => $request->link,
            'logo' => $request->logo,
        ]);

        return redirect(route('company.index'))->with('success', 'Data company telah berjaya dimuat-naik');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function data(Request $req)
    {
        $data = company::all();

        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = company::where('id',$id)->first();
        return view('edit',compact(['data']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);

        $data = company::where('id',$id)->first();

        if($request->hasFile('logo')){
        
            $destination = 'uploads/'.$data->logo;

            // dd($destination);

            if(File::exists($destination)){

                File::delete($destination);
            }
            
            $filename = $request->name.'.'.$request->file('logo')->getClientOriginalExtension();
            $request->logo =  $filename;
            
            $file = $request->file('logo');
            $file->move('uploads/', $filename);
            
        }else{
            
            $request->logo = $data->logo;
        }

        company::where('id',$id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'link' => $request->link,
            'logo' => $request->logo,
        ]);

        return redirect(route('company.index'))->with('success', 'Data company telah berjaya diKemaskini');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = company::where('id',$id)->first();
        // dd($data);
        $data->delete();

        return redirect(route('company.index'))->with('success', 'Data company berjaya diPadamkan');

    }
}
