<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Crud') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if (Session::has('success'))
                        <div class="w-100">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ Session::get('success') }}
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                              </div>
                        </div>
                    @endif
                    <!-- Button trigger modal -->
                    <a href="{{ route('company.create') }}" type="button" class="btn btn-primary btn-sm">
                        Add new Company
                    </a>
                    
                </div>

                <div class="p-3">
                    <table id="company" class="table table-sm  table-responsive w-100">
                        <thead>
                            <tr>
                                <th class="text-left">Name</th>
                                <th class="col "></th>
                                <th class="col ">Email</th>
                                <th class="col ">Link</th>
                                <th class="col text-right"></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    {{-- <div class="modal fade" id="edit" tabindex="-1">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title test-gray-700">Edit Company</h5>
            <button type="button" id="saveE" class="btn btn-success btn-sm">Simpan</button>
            </div>
            <div class="modal-body">
                <h6 class="uppercase text-gray-400">Company information</h6>
                <div class="row">
                    <div class="my-2 mx-auto w-50 logoH">
                        <img id="imgPreview" src="" alt="" srcset="" class="logoH">
                    </div>
                </div>
                <form action="{{ route('company.update',['id'=>'']) }}" method="patch" enctype="multipart/form-data">
                    <div class="row">
                        <input type="hidden" name="idE" class="form-control form-control-sm" id="idE">
                        <div class="col-lg-6">
                            <label for="name" class="form-label">Name:</label>
                            <input type="text" name="nameE" class="form-control form-control-sm" id="nameE">
                        </div>
                        <div class="col-lg-6">
                            <label for="email" class="form-label">Email:</label>
                            <input type="text" name="emailE" class="form-control form-control-sm" id="emailE">
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-lg-6">
                            <label for="logo" class="form-label">Logo:</label>
                            <input id="photo" type="file" name="logoE" class=" form-control form-control-sm " id="logoE" >
                            
                        </div>
                        
                        <div class="col-lg-6">
                            <label for="linkE" class="form-label">Website:</label>
                            <input type="text" name="linkE" class="form-control form-control-sm" id="linkE">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </div> --}}
    @section('js')

        <script>

            var AP = [];
            var datacom = null;
            
            $(document).ready( function () {

                $.ajax({
                    'url':'/companyData',
                    'type':'POST',
                    'data':{
                        '_token': '{{ csrf_token() }}',
                    },
                    success:function(response){
                        console.table(response);
                        if(response){
                            datacom = response;
                            // AP.table.destroy();
                            AP.table =  $('#company').DataTable({
                                "data": datacom,
                                "processing":true,
                                "bLengthChange" : false,
                                "pageLength": 15,
                                "ordering": false,
                                "paging" : false,
                                "info" : false,
                                "searching":false,
                                "columns": [
                                    { "data": "logo", defaultContent: '', width: '20px', className: 'text-center',

                                        render: function ( data, type, row ) {
                                            return ` 
                                                    <img id="preview_img" src="uploads/`+data+`" alt="NoPhoto" class="rounded-3 iconimg">

                                                    `;
                                        }

                                    },
                                    { "data": "name", defaultContent: '', width: '30px', className: 'text-left'},
                                    { "data": "email", defaultContent: '', width: '30px', className: 'text-left' },
                                    { "data": "link", defaultContent: '', width: '30px', className: 'text-left' },
                                    {
                                        "data": "id",defaultContent:"" ,className: 'text-center', width: '30px',
                                        render: function ( data, type, row ) {
                                            return ` <button> 
                                                        <a href="/company/`+data+`/edit">
                                                        Edit
                                                        </a> 
                                                    </button>
                                                    <form action="/company/`+data+`" method="POST"
                                                        class="d-inline">
                                                        @csrf
                                                        @method('delete')
                                                        <button class="text-danger"
                                                            onclick="return confirm('Are you sure?')">Delete</button>
                                                    </form>
                                                    `;
                                        }

                                    },

                                ]
                            });

                            AP.table.on('click', 'button', function(){
                                AP.selected = AP.table.row($(this).parents('tr')).data();

                                console.log(AP.selected);
                                $('#idE').val(AP.selected.id);
                                $('#nameE').val(AP.selected.name);
                                $('#emailE').val(AP.selected.email);
                                $('#imgPreview').attr("src", "uploads/"+AP.selected.logo);
                                $('#linkE').val(AP.selected.link);


                            });
                        }
                    }

                });

                $('#photo').change(function(){
                    const file = this.files[0];
                    console.log(file);
                    if (file){
                    let reader = new FileReader();
                    reader.onload = function(event){
                        console.log(event.target.result);
                        $('#imgPreview').attr('src', event.target.result);
                    }
                    reader.readAsDataURL(file);
                    }
                });

            });
        
        </script>

    @stop
</x-app-layout>


