<x-app-layout>

    <div class="card col-lg-6 mx-auto mt-2">
        <div class="card-body ">

            <form action="{{ route('company.update',['company' => $data->id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-8">
                        <h6 class="uppercase text-gray-400">Edit Company information</h6>
                    </div>
                    <div class="col-4 text-right">
                        <button type="submit"class="btn btn-success btn-sm">Save</button>
                    </div>
                </div>
                <div class="row">
                    <div class="my-2 mx-auto w-50 max-h-3 ">
                        {{-- C:\laragon\www\2QsoftwareDevTecAss\public\uploads\default.jpg --}}
                        <img id="preview_img" src="{{url('uploads/'.$data->logo)}}" alt="NoPhoto" class="w-100 h-100 logoAdd">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <label for="name" class="form-label">Name:</label>
                        <input type="text" name="name" class="form-control form-control-sm" id="name" value="{{ $data->name }}">
                        @error('name')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <label for="email" class="form-label">Email:</label>
                        <input type="text" name="email" class="form-control form-control-sm" id="email" value="{{ $data->email }}">
                        @error('email')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-lg-6">
                        <label for="logo" class="form-label">Logo:</label>
                        <input type="file" name="logo" class=" form-control form-control-sm " id="logo">
                        <p class="text-danger">*png only </p>
                        @error('logo')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <label for="link" class="form-label">Website:</label>
                        <input type="text" name="link" class="form-control form-control-sm" id="link" value="{{ $data->link }}">
                        @error('link')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
            </form>
        </div>
    </div>

    @section('js')

        <script>

            $(document).ready( function () {

                $('#logo').change(function(){
                    const file = this.files[0];
                    console.log(file);
                    if (file){
                    let reader = new FileReader();
                    reader.onload = function(event){
                        console.log(event.target.result);
                        $('#preview_img').attr('src', event.target.result);
                    }
                    reader.readAsDataURL(file);
                    }
                });

            });

        </script>

    @stop
</x-app-layout>


