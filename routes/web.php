<?php

use App\Http\Controllers\CompanyController;
use App\Http\Resources\CompanyResource;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::resource('company', CompanyController::class)->middleware('auth');

Route::post('/companyData', [CompanyController::class,'data'])->middleware('auth')->name('data');


require __DIR__.'/auth.php';
